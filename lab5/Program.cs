﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinqToObjects
{
    class Program
    {
        static void Main(string[] args)
        {
            var numbers = new List<string>
            {
                "zero",
                "one",
                "two",
                "three",
                "four",
                "five",
                "six",
                "seven",
                "eight",
                "nine",
                "nine",
            };

            Console.WriteLine("1. Strings with substring \"ne\"");

            var substring = "ne";

            var q1 = from number in numbers
                     where number.Contains(substring)
                     select number;
            Print(q1);

            Console.WriteLine("\n2. Ordered strings");

            var q2 = from number in numbers
                     orderby number
                     select number;
            Print(q2);

            Console.WriteLine("\n3. Count of unique strings");
            Console.WriteLine(numbers.Distinct().Count());

            Console.WriteLine("\n4. Grouping");

            var q3 = from number in numbers
                     group number by number.Length into g
                     select new { Length = g.Key, Count = g.Count() };
            
            foreach (var obj in q3)
            {
                Console.WriteLine($"Length: {obj.Length} | Count: {obj.Count}");
            }
        }

        static void Print(IEnumerable<string> strings)
        {
            foreach (var s in strings)
            {
                Console.WriteLine(s);
            }
        }
    }
}
