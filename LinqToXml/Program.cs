﻿using System;
using System.Linq;
using System.Xml.Linq;

namespace LinqToXml
{
    class Program
    {
        static void Main(string[] args)
        {
            var path = "../../../scheme.xml";
            var root = XElement.Load(path);

            Console.WriteLine("1. List of clients by hotel");

            var q1 = (from hotel in root.Elements("hotel")
                    where hotel.Element("name").Value == "Home like"
                    select hotel.Elements("persons").Elements("person")).First();

            foreach (var person in q1)
            {
                Console.WriteLine($"Name: {person.Value} | Num: {person.Attribute("num").Value}");
            }

            Console.WriteLine("\n2. Creating, editing and deleting client of hotel");

            var q2 = (from hotel in root.Elements("hotel")
                     where hotel.Element("name").Value == "Home like"
                     select hotel).First();

            Console.WriteLine($"* Initial hotel:\n{q2}");

            q2.Element("persons").Add(new XElement("person", "New Client", new XAttribute("num", 33)));
            Console.WriteLine($"* Creating client:\n{q2}");

            q2.Elements("persons").Elements("person").Last().Value = "New Name";
            Console.WriteLine($"* Editing client:\n{q2}");

            q2.Elements("persons").Elements("person").Last().Remove();
            Console.WriteLine($"* Removing client:\n{q2}");

            Console.WriteLine("\n3. Hotel count by city");

            var q3 = from hotel in root.Elements("hotel")
                     group hotel by hotel.Attribute("city").Value into g
                     select new { City = g.Key, Count = g.Count() };

            foreach (var val in q3)
            {
                Console.WriteLine($"City: {val.City} | Count = {val.Count}");
            }
        }
    }
}
