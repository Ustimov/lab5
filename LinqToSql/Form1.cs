﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Xml.Linq;

namespace LinqToSql
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'linqToSqlDataSet.Hotel' table. You can move, or remove it, as needed.
            this.hotelTableAdapter.Fill(this.linqToSqlDataSet.Hotel);
            // TODO: This line of code loads data into the 'linqToSqlDataSet.Client' table. You can move, or remove it, as needed.
            this.clientTableAdapter.Fill(this.linqToSqlDataSet.Client);

        }

        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {
                this.Validate();
                this.clientBindingSource.EndEdit();
                this.clientTableAdapter.Update(this.linqToSqlDataSet.Client);
                MessageBox.Show("Update successful");
            }
            catch (Exception ex)
            {
                MessageBox.Show("Update failed");
            }
        }

        private void fourAndFileStarsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var q = (from hotel in this.linqToSqlDataSet.Hotel
                    where hotel.hcountry == "Russia" && 
                    (hotel.stars == 4 || hotel.stars == 5)
                    select hotel).AsDataView();

            var form2 = new Form2();
            form2.Fill(q);
            form2.Show();
        }

        private void clientsOfRussianHotelsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var q = (from client in this.linqToSqlDataSet.Client
                     where client.HotelRow.hcountry == "Russia"
                     orderby client.fio
                     select new {
                         client.age,
                         client.country,
                         client.fio,
                         client.id,
                         client.num,
                         client.ofhotel,
                         client.HotelRow.stars,
                         client.HotelRow.hcountry,
                         client.HotelRow.hcity,
                         client.HotelRow.hname}).ToList();
            
            var form2 = new Form2();
            form2.Fill(q);
            form2.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void hotelsWith2AndMoreClientsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var q = (from hotel in this.linqToSqlDataSet.Hotel
                     let clients = hotel.GetClientRows().Length
                     where clients > 1
                     orderby clients
                     select new { hotel.hname, hotel.hcity, clients }).ToList();

            var form2 = new Form2();
            form2.Fill(q);
            form2.Show();
        }

        private void noAmericansToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var q = this.linqToSqlDataSet.Client.Where(c1 => 
                c1.HotelRow.GetClientRows().All(c2 => c2.country != "USA")).AsDataView();

            var form2 = new Form2();
            form2.Fill(q);
            form2.Show();
        }

        private void saveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var saveFileDialog = new SaveFileDialog()
            {
                AddExtension = true,
                Filter = "XML files (*.xml)|*.xml",
            };

            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                File.WriteAllText(saveFileDialog.FileName, GenerateXml());
            }
        }

        private string GenerateXml()
        {
            var hotels = new List<XElement>();
            foreach (var hotel in this.linqToSqlDataSet.Hotel)
            {
                var persons = new List<XElement>();
                foreach (var client in hotel.GetClientRows())
                {
                    var person = new XElement("person", client.fio);
                    person.Add(new XAttribute("num", client.num));
                    persons.Add(person);
                }

                var h = new XElement("hotel", new XElement("name", hotel.hname));
                h.Add(new XElement("persons", persons));
                h.Add(new XAttribute("star", hotel.stars));
                h.Add(new XAttribute("city", hotel.hcity));
                h.Add(new XAttribute("country", hotel.hcountry));
                hotels.Add(h);
            }
            var root = new XElement("hotels", hotels);
            return root.ToString();
        }

        private void selectAllToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new Form3().Show();
        }
    }
}
