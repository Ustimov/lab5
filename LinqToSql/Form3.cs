﻿using System;
using System.Linq;
using System.Windows.Forms;

namespace LinqToSql
{
    public partial class Form3 : Form
    {
        private DataClasses1DataContext _ctx;

        public Form3()
        {
            InitializeComponent();
            _ctx = new DataClasses1DataContext();
        }

        private void createButton_Click(object sender, EventArgs e)
        {
            try
            {
                var fio = fioTextBox.Text;
                var num = int.Parse(numTextBox.Text);
                var age = int.Parse(ageTextBox.Text);
                var country = countryTextBox.Text;
                var ofhotel = ofhotelTextBox.Text;

                var client = new Client
                {
                    fio = fio,
                    num = num,
                    age = age,
                    country = country,
                    ofhotel = ofhotel
                };
                _ctx.Clients.InsertOnSubmit(client);
                _ctx.SubmitChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void readButton_Click(object sender, EventArgs e)
        {
            try
            {
                var id = int.Parse(idTextBox.Text);
                var client = _ctx.Clients.Where(cl => cl.id == id).Single();
                //var client = clientTableAdapter1.GetData().FindByid(id);
                fioTextBox.Text = client.fio;
                numTextBox.Text = client.num.ToString();
                ageTextBox.Text = client.age.ToString();
                countryTextBox.Text = client.country;
                ofhotelTextBox.Text = client.ofhotel;
                //_ctx.Clients.InsertOnSubmit(client);
                
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void updateButton_Click(object sender, EventArgs e)
        {
            try
            {
                var id = int.Parse(idTextBox.Text);
                //var client = clientTableAdapter1.GetData().FindByid(id);
                var client = _ctx.Clients.Where(cl => cl.id == id).Single();
                var fio = fioTextBox.Text;
                var num = int.Parse(numTextBox.Text);
                var age = int.Parse(ageTextBox.Text);
                var country = countryTextBox.Text;
                var ofhotel = ofhotelTextBox.Text;
                //clientTableAdapter1.Update(fio, num, age, country, ofhotel,
                //    client.fio, client.num, client.age, client.country,
                //    client.ofhotel, client.id);
                client.fio = fio;
                client.num = num;
                client.age = age;
                client.country = country;
                client.ofhotel = ofhotel;
                _ctx.SubmitChanges();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void deleteButton_Click(object sender, EventArgs e)
        {
            try
            {
                var id = int.Parse(idTextBox.Text);
                var client = _ctx.Clients.Where(cl => cl.id == id).Single();
                _ctx.Clients.DeleteOnSubmit(client);
                _ctx.SubmitChanges();
                //var client = clientTableAdapter1.GetData().FindByid(id);
                //clientTableAdapter1.Delete(client.fio, client.num, client.age,
                //    client.country, client.ofhotel, client.id);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void clientBindingNavigatorSaveItem_Click(object sender, EventArgs e)
        {
            this.Validate();
            this.clientBindingSource.EndEdit();
            this.tableAdapterManager.UpdateAll(this.linqToSqlDataSet1);

        }
    }
}
