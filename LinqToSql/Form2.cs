﻿using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace LinqToSql
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        public void Fill(DataView dataView)
        {
            dataGridView1.DataSource = dataView;
        }

        public void Fill<T>(IEnumerable<T> rows)
        {
            dataGridView1.DataSource = rows;
        }
    }
}
